# Learning Jupyter

## Starting Jupyter Lab in Docker
```
docker run -it -p 8888:8888 -v ./local-jupyter-files:/home/jovyan/work jupyter/scipy-notebook
```
